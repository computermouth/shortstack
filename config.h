
#ifndef _CONFIG_H_
#define _CONFIG_H_

int init_config();
int check_config();
void save_config();

#endif
